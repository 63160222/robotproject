/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.robotproject2;

/**
 *
 * @author MSI GAMING
 */
public class TableMap {

    private int width;
    private int heigth;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int heigth) {
        this.width = width;
        this.heigth = heigth;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showTitle();
        for (int y = 0; y < heigth; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.inOn(x, y)) {
                    showRobot();
                } else if(bomb.inOn(x, y)) {
                    showBomb();
                }else{
                    showCell();
                }

            }
            showNewLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < heigth);
    }

    public boolean inBomb(int x, int y) {
        return bomb.inOn(x, y);
    }
}
